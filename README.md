# Convenience Launcher Chrome Extension
No bells and whistles here--this is just a "lazy" tool for myself...

## Installation
1. In the Location Bar in a running instance of Google Chrome, `chrome://extensions`
1. Check the "Developer mode" box at the top-right
1. "Load unpacked extension..."
	- Just add the containing folder for the entire project rather than any specific file(s).

An icon should appear in the extensions bar. If it doesn't appear, try refreshing the page first.


## Usage
1. Copy `config.json.sample` to `config.json` (in the same directory).
1. Follow the "Configuration" instructions (although it's fairly self-documenting).
1. Click on the extension's icon in the Extension toolbar and a small popup will appear with the (parsed) contents of your `config.json` file.

### Simple
If you click on a regular, non-group link (one which does *not* have "(+)" at the end / is directly mapped to a location, rather than an object), then that link's location will be opened in a new tab and you will be taken to it.

### Advanced
If you click on a grouping link (one which *does* have "(+)" at the end / is has another object as its value), then all of its descendants will open in new tabs.

For instance, in the "Advanced" config example above, if any of the "Grouped Link xx" are clicked then only the clicked one will open. Meanwhile, if "Subgroup 3" gets clicked, then "Grouped Link" 10, 11, and 12 will open. Similarly, a click on "Group 1" will open all links except for "Link 1" and "Link 2".

> Note: Due to the functionality outlined in "Usage::Advanced", use your best judgement when opening deeply-nested Groups. I've personally opened 36 (4 locations copied into 9 nested groups) at once without any lag at all and could see it easily scaling to any reasonable expectation due to the simplicity of its approach. With that being said, be sure to consider your RAM/CPU before doing so. I've also (to test this claim) opened exactly 400 at once in its own windows (yes, exactly 400--the same 4 tabs copied into 100 nested groups) and while this did not lock up my machine, it *did* lock up Chrome for a considerable amount of time while it tried to close everything that was opened (which took quite some time (5-10 minutes)...and around 12-16GB of RAM (mostly swap space) excluding usage from my typical workflow tools before the test (2 Chrome sessions (personal/business), 10 terminals (tmux), 2 Sublime sessions, 1 PHPStorm, 1 desktop chat client, 1 desktop email client, and 1 desktop calendar client)).


## Configuration
Since I don't currently care enough to make a "settings" feature for this (although I'm sure having import/export/cloud-shared settings with Google Account integration would be great at some point...some day...), here's this in the mean time...

Just edit the `config.json` and whenever you refresh the page/load a new page, the extension should be autoamtically updated.

### Simple
```json
{
	"links": {
		"Link 1": "location_of_link_like_http",
		"Group 1": {
			"Grouped Link 1": "location",
			"Grouped Link 2": "location",
			"Grouped Link 3": "location"
		},
		"Link 2": "location"
	}
}
```

### Advanced
```json
{
	"links": {
		"Link 1": "location_of_link_like_http",
		"Group 1": {
			"Grouped Link 1": "location",
			"Grouped Link 2": "location",
			"Grouped Link 3": "location",
			"Subgroup 1": {
				"Grouped Link 4": "location",
				"Grouped Link 5": "location",
				"Grouped Link 6": "location",
				"Subgroup 2": {
					"Grouped Link 7": "location",
					"Grouped Link 8": "location",
					"Grouped Link 9": "location",
					"Subgroup 3": {
						"Grouped Link 10": "location",
						"Grouped Link 11": "location",
						"Grouped Link 12": "location"
					}
				}
			}
		},
		"Link 2": "location"
	}
}
```
