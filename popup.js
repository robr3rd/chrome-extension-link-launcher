var MainPopup = function() {
	var self = this;

	self.config = null;

	/**
	 * Retrieve config file contents and parse it as an object
	 *
	 * @param  {Function} callback A callback...just...a callback
	 * @return {void}              Nothing is returned (although MainPopup.config gets set!)
	 */
	self.importConfig = function(callback) {
		$.getJSON(chrome.extension.getURL('/config.json'), function(data) {
			self.config = data;
			callback();
		});
	};

	/**
	 * Render one "generation" (i.e. indentation level) at a time, recursively
	 *
	 * @param  {object} links JSON object representing a "generation" (siblings) of links to render
	 * @return {string}       HTML representation of the passed object in the form of nested unordered lists
	 */
	self.renderLinks = function(links) {
		var output = '';

		for (var link in links) {
			if (links.hasOwnProperty(link)) {
				if (typeof links[link] === 'object') {
					output += '<li class="group"><a href="#">' + link + ' (+)</a>' +
						'<ul>' +
						self.renderLinks(links[link]) + // Here lie recursion
						'</ul>';
				} else {
					output += '<li class="link"><a target="_blank" href="' + links[link] + '">' + link + '</a>';
				}

				output += '</li>';
			}
		}

		return output;
	};

	/**
	 * Add onclick for Groups to auto-open direct children
	 *
	 * @return {void} Adds event listeners for the Group links since they are just containers that recursively trigger their children
	 */
	self.addListeners = function() {
		$(document).ready(function() {
			$('.group').each(function() { // this = group `li` (each)
				var self = this;

				$(this).find('> a').each(function() { // this = group `a` (find -> each)
					$(this).click(function() { // this = group `a` (click)
						$(self).find('ul li.link > a').each(function() { // this = link `a` (find -> each)
							// $(this).trigger('click');
							window.open($(this).attr('href'), '_blank'); // this = group or link (child "a" of the above group)
						});
					});
				});
			});
		});
	};

	/**
	 * Generate content for the popup box
	 *
	 * @return {void} Inserts HTML into the popup's body
	 */
	self.render = function() {
		if (self.config === null) {
			self.importConfig(self.render); // Self as callback to prevent race condition
		} else {
			$('#list').html('<ul>' + self.renderLinks(self.config.links) + '</ul>');
			self.addListeners();
		}

		$('body').css('height', $('#list').height() + 'px');
		$('body').css('width', $('#list').width() + 'px');
	};
};

var Popup = new MainPopup();
Popup.render();
